#this one creates certificate for users
#Check whether the Cetification Authority exists or not. Not creating certificate authority here, else different Principal might end up creating different CAs
cd ./nodes
echo "running create_cert"
cd CA
if !  test -d Certificate_Authority ; then 
	echo "Certificate Authority do not exist. Please create one" 
	exit 1
fi
cd ..
#Generating Certificates
user=$1
if   test -d $user -a -d ./CA/Certificate_Authority/$user  ; then 
	echo "Principal already created" 
	exit 0
fi
# mkdir -p ./$user
#cd $user
openssl genrsa -out ./$user/RSA.key 2048
#to export the public key
openssl rsa -in ./$user/RSA.key -outform PEM -pubout -out ./$user/public.pem
# echo "after creating public key"
# ls
openssl req -new -key ./$user/RSA.key -out $user/req.csr -subj "/C=IN/ST=UP /L=Kanpur/O="$user"/OU=CSE/CN="$user
openssl x509 -req -in ./$user/req.csr -CA ./CA/Certificate_Authority/rootCA.pem -CAkey ./CA/Certificate_Authority/rootCA.key -CAcreateserial -out ./$user/certificate.crt -days 500 -sha256 -passin pass:1234
mkdir ./CA/Certificate_Authority/$user

#copying the certificate to certification authority for future needs
cp ./$user/certificate.crt ./CA/Certificate_Authority/$user
# echo "Displaying the certificates "
# echo $pwd
# openssl x509 -text -noout -in ./$user/$user.crt
if [ "$?" = "0" ]; then
	echo "Certificates created successfuly for user " $user
else
	echo "Error in certificates creation"
fi


