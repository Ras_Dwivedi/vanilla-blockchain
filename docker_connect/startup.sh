#take argument here and then use that to clear all the files
python ./gen_peer.py
docker-compose build
docker-compose up -d
./getIP.sh
echo "Type exit to shutdown the network"
flag="false"
while [ $flag = "false" ] ; do
	read input
	if [ $input = "exit" ] ; then 
		echo "Shutting down the Network" 
		./shutdown.sh
		flag="true"
	fi
done 

if [ "$?" = "0" ]; then
	echo "Containers succesfully exited " $user
else
	echo "Error in exit"
fi

