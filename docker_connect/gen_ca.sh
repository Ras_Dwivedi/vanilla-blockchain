mkdir nodes
cd nodes
mkdir CA
cd CA
mkdir Certificate_Authority
cd Certificate_Authority
echo $PWD
openssl genrsa -out ./rootCA.key -aes256 -passout pass:1234 2048
openssl req -x509 -new -nodes -key ./rootCA.key -sha256 -days 1024 -out ./rootCA.pem -subj "/C=IN/ST=UP/L=Kanpur/O=IITK/OU=Certification Authority/CN=CA" -passin pass:1234
