import requests
import time
import os
#creating folders and certificate aurthority
os.system("./gen_ca.sh") 
#make sure that it creates nested folder
# now create certificates
n=raw_input("enter the number of peers to be generated:   ")
for i in range (int(n)):
        dir_name="p_"+str(i)
        # os.system("mkdir "+dir_name)
        os.system("cp -r ./files/. ./nodes/"+dir_name+"/")
        os.system("./create_cert.sh "+dir_name) 
        #appropriate changes should be reflected here 

#writing docker-compose file below

f=open("docker-compose.yml", "w+")
f.write("version: '3' \n")
#Naming the network
f.write("networks:\n")
f.write("  vbc:\n") # for vanilla blockchain
# Writing the code for Certificate Authority
os.system("cp -r ./ca/. ./nodes/CA")
f.write("services:\n")
f.write("  ca: \n")
f.write("    build: ./nodes/CA/\n")
f.write("    ports:\n")
portno=5000
f.write('      - "'+str(portno)+':'+str(portno)+'" \n')
f.write("    volumes:\n")
f.write("      - ./IPAddress:/Desktop/IPAddress/\n")
f.write("      - ./logicchain:/Desktop/logicchain/\n")
f.write("    container_name: certificate_authority\n")
f.write("    networks:\n")
f.write("      - vbc\n")
# writing the code for peers
for i in range(int(n)):
        dir_name="p_"+str(i)
        f.write("  p"+ str(i)+": \n")
        f.write("    build: ./nodes/p_"+str(i)+"/\n")
        f.write("    ports:\n")
        portno=5001+i
        f.write('      - "'+str(portno)+':'+str(portno)+'" \n')
        f.write("    volumes:\n")
        f.write("      - ./IPAddress:/Desktop/IPAddress/\n")
        f.write("      - ./logicchain:/Desktop/logicchain/\n")
        f.write("    container_name: peer"+str(i)+"\n")
        f.write("    networks:\n")
        f.write("      - vbc\n")
f.close()
