This repository is for creating docker based vanilla blockchain.
WORK DONE TILL NOW
1. It can now create N containers, all identical that could serve as node of Blockchain
2. Along with these N container, it also creates a Certificate Authority, that stores certficates of all the conatiners, and then could be used for verification
3. All N nodes, now are derived from open-jdk, have installed sbt, scala riple and python.
4. All nodes know the IP address of other nodes and could send direct message to each other. Till now any string message coud be broadcasted


WORK TO BE DONE
1. modify the WriteObject code, so as to write any verified logical statements
2. modify it in such a way, so that all the statements are stored in form of P says s
3. Since the previous verision have the classes P says s , you need to modify AST.scala so to remove those expressions, and then introduce another form of derivations
4. One need to figure out how can one communicate individual statements, vs the entire block. May be you can use another port and another instance of Server.py
5. client.py need to be modified, so that the recieved object is properly stored
6. Need to create some form of consensus mechanism

